package dp.creational.singleton;

public class Campus {
	private String name;
	private static Campus campus = null;

	private Campus() {

	}

	public static Campus createInstance() {
		if (campus == null) {
			campus = new Campus();
		}
		return campus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Campus [name =" + name + "]";
	}

	public static void setNull() {
		campus = null;
	}
}
