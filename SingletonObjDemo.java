package dp.creational.singleton;

public class SingletonObjDemo {

	public static void main(String[] args) {
		// SingletonObj obj = new SingletonObj();

		SingletonObj obj = SingletonObj.getInstance();
		System.out.println(obj);

		obj.setNull();

		SingletonObj obj2 = SingletonObj.getInstance();
		System.out.println(obj2);

	}
}

/*
 * SingletonObj obj = SingletonObj.getInstance(); System.out.println(obj + " , "
 * + obj.hashCode()); SingletonObj obj2 = SingletonObj.getInstance();
 * System.out.println(obj + " , " + obj.hashCode()); OUTPUT:
 * dp.creational.singlelton.SingletonObj@3830f1c0 , 942731712
 * dp.creational.singlelton.SingletonObj@3830f1c0 , 942731712
 * 
 * obj.setNull(); dp.creational.singlelton.SingletonObj@3830f1c0
 * dp.creational.singlelton.SingletonObj@39ed3c8d
 * 
 * //obj.setNull(); dp.creational.singlelton.SingletonObj@3830f1c0
 * dp.creational.singlelton.SingletonObj@3830f1c0
 * 
 */