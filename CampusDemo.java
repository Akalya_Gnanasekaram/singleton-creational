package dp.creational.singleton;

public class CampusDemo {
	public static void main(String[] args) {

		Campus jaffna = Campus.createInstance();
		jaffna.setName("Jaffna");
		System.out.println(jaffna + " " + jaffna.hashCode());

		jaffna.setNull();

		Campus colombo = Campus.createInstance();
		colombo.setName("Colombo");
		System.out.println(colombo + " " + colombo.hashCode());
	}

}

/*
 * //jaffna.setNull(); Campus [name =Jaffna] 942731712 Campus [name =Colombo]
 * 942731712
 * 
 * jaffna.setNull(); Campus [name =Jaffna] 942731712 Campus [name =Colombo]
 * 971848845
 * 
 */
