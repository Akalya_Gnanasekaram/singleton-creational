package dp.creational.singleton;

public class SingletonObj {

	private static SingletonObj singletonobj;

	private SingletonObj() {

	}

	public static SingletonObj getInstance() {
		if (singletonobj == null) {
			singletonobj = new SingletonObj();
		}
		return singletonobj;
	}

	public static void setNull() {
		singletonobj = null;

	}

}
